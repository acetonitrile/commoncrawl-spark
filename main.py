from pyspark.sql import SparkSession
def get_spark():
    spark = SparkSession.builder.master("local[4]").appName('SparkDelta') \
        .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
        .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
        .config("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem") \
        .config("spark.jars.packages", 
                "io.delta:delta-core_2.12:2.1.0,"
                "org.apache.hadoop:hadoop-aws:3.3.3,"
                "com.amazonaws:aws-java-sdk-bundle:1.12.262") \
        .enableHiveSupport() .getOrCreate()
    return spark
# This is mandate config on spark session to use AWS S3
spark = get_spark()

hadoopConf = spark._jsc.hadoopConfiguration()

AWS_ACCESS_KEY = 'adsjkfdjks'
AWS_SECRET_KEY = 'dfdsiufhbdsik'
hadoopConf.set("fs.s3a.access.key", AWS_ACCESS_KEY)
hadoopConf.set("fs.s3a.secret.key", AWS_SECRET_KEY)
hadoopConf.set("com.amazonaws.services.s3.enableV4", "true")
hadoopConf.set("fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
hadoopConf.set("fs.s3a.aws.credentials.provider","com.amazonaws.auth.InstanceProfileCredentialsProvider,com.amazonaws.auth.DefaultAWSCredentialsProviderChain")
hadoopConf.set("fs.AbstractFileSystem.s3a.impl", "org.apache.hadoop.fs.s3a.S3A")
# spark.sparkContext.setLogLevel("DEBUG")

cc_index_create_query = ''
with open('cc-index-create.sql') as f:
    cc_index_create_query = f.read()

res = spark.sql(cc_index_create_query)
res
